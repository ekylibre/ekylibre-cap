# coding: utf-8

require 'nokogiri'

module CAP
  class << self
    def abaci_dir
      Ekylibre.root.join('plugins', 'cap', 'abaci')
    end
  end
end
