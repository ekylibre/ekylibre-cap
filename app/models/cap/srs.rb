module CAP
  class SRS
    cattr_reader :spatial_references
    @@spatial_references = Abaci.read(CAP.abaci_dir.join('srs.csv'))

    class << self
      def find_srid(insee_code)
        srid = 2154
        row = ::CAP::SRS.spatial_references.where(DEP: insee_code[0..2], COM: insee_code[3..4].to_i.to_s)
        srid = row.first.SRID.to_i if row.size == 1
        srid
      end
    end
  end
end
