require 'nokogiri'

module CAP
  class TelepacFile
    # return variety from main_crop_code & main_crop_precision of CapLandParcel object
    # parse all abaci during 15 cases
    def self.find_variety(cap_land_parcel, cap_year = 2018)
      # VARIABLES
      #
      variety = nil
      files = []
      complex_files = []

      # TEST in rails c
      # Ekylibre::Tenant.switch! 'demo'
      # c = CapLandParcel.first
      # Ekylibre::Plugin::CAP::TelepacFile.find(c)

      # CASES
      #
      # case 01 - CEREALS
      file = "#{CAP.abaci_dir}/v#{cap_year}/1-01-cereals.csv"
      f = File.open(file)
      table = Abaci.read(f)
      row = table.where(name: cap_land_parcel.main_crop_code)
      if row.present? && row.try(:precision?) && !cap_land_parcel.try(:main_crop_precision?)
        sub_file = "#{CAP.abaci_dir}/v#{cap_year}/#{row.precision}.csv"
        if row.precision.to_s =~ 'durum_wheat_varieties' || row.precision.to_s =~ 'common_wheat_varieties'
          sub_f = File.open(sub_file)
          sub_table = Abaci.read(sub_f)
          sub_row = sub_table.where(code: cap_land_parcel.main_crop_precision)
          variety = sub_row.first.variety.to_sym
        elsif row.precision.to_s =~ 'mixing_types'
          variety = :poaceae
        end
      elsif row.present? && !row.try(:precision?)
        # return a request like "is triticum"
        # TODO how to treat this request to have a variety
        variety = row.first.variety.to_sym
      end

      # case 02 - OLEAGINOUS
      file = "#{CAP.abaci_dir}/v#{cap_year}/1-02-oleaginous.csv"
      f = File.open(file)
      table = Abaci.read(f)
      row = table.where(name: cap_land_parcel.main_crop_code)
      if row.present? && row.name != 'MOL'
        variety = row.first.variety.to_sym
      elsif row.present? && row.name == 'MOL'
        variety = :plant
      end

      # case 03 - PROTEAGINOUS
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-03-proteaginous.csv"

      # case 04 - FIBER PLANT
      complex_files << "#{CAP.abaci_dir}/v#{cap_year}/1-04-fiber_culture.csv"

      # case 05 - FALLOW LAND
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-05-fallow_land.csv"

      # case 06 - LEGUMINOUS
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-06-leguminous.csv"

      # case 07 - FODDER LEGUMINOUS
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-07-fodder_leguminous.csv"

      # case 08 - FODDER
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-08-fodder.csv"

      # case 09 - TEMPORARY MEADOW
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-09-temporary_meadow.csv"

      # case 10 - PERMANENT MEADOW
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-10-pasture.csv"

      # case 11 - FRUITS AND VEGETABLES
      complex_files << "#{CAP.abaci_dir}/v#{cap_year}/1-11-fruits_and_vegetables.csv"

      # case 12 - ARBORICULTURE AND VITICULTURE
      complex_files << "#{CAP.abaci_dir}/v#{cap_year}/1-12-arboriculture_viticulture.csv"

      # case 13 - ORNEMENTAL PLANT
      complex_files << "#{CAP.abaci_dir}/v#{cap_year}/1-13-ornamental_plants.csv"

      # case 14 - MISCELLANEOUS
      file = "#{CAP.abaci_dir}/v#{cap_year}/1-14-miscellaneous.csv"
      f = File.open(file)
      table = Abaci.read(f)
      row = table.where(name: cap_land_parcel.main_crop_code)
      if row.present? && row.try(:precision?) && !cap_land_parcel.try(:main_crop_precision?)
        sub_file = "#{CAP.abaci_dir}/v#{cap_year}/#{row.precision}.csv"
        if row.precision.to_s =~ 'thicket_varieties'
          sub_f = File.open(sub_file)
          sub_table = Abaci.read(sub_f)
          sub_row = sub_table.where(code: cap_land_parcel.main_crop_precision)
          variety = sub_row.first.variety.to_sym
        elsif row.precision.to_s
          variety = row.first.variety.to_sym
        end
      end

      # case 15 - TROPICAL PLANT
      complex_files << "#{CAP.abaci_dir}/v#{cap_year}/1-15-tropical_plants.csv"

      # METHODS
      #
      # simple file method to grab variety
      for file in files
        f = File.open(file)
        table = Abaci.read(f)
        row = table.where(name: cap_land_parcel.main_crop_code)
        if row.present?
          variety = row.first.variety.to_sym
        end
      end

      # complex file method to grab variety
      for file in complex_files
        f = File.open(file)
        table = Abaci.read(f)
        row = table.where(name: cap_land_parcel.main_crop_code)
        if row.present? && row.try(:precision?) && !cap_land_parcel.try(:main_crop_precision?)
          sub_file = "#{CAP.abaci_dir}/v#{cap_year}/#{row.precision}.csv"
          sub_f = File.open(sub_file)
          sub_table = Abaci.read(sub_f)
          sub_row = sub_table.where(code: cap_land_parcel.main_crop_precision)
          variety = sub_row.first.variety.to_sym
        elsif row.present?
          variety = row.first.variety.to_sym
        end
      end

      # RESULT
      #
      variety
    end

    # return a row
    def self.find(cap_land_parcel, cap_year=nil)
      # VARIABLES
      #
      cap_year ||= 2019


      files = []

      # TEST in rails c
      # Ekylibre::Tenant.switch! 'earl-deplat'
      # c = CapLandParcel.first
      # Ekylibre::Plugin::CAP::TelepacFile.where(c)

      # CASES
      #
      # case 01 - CEREALS
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-01-cereals.csv"

      # case 02 - OLEAGINOUS
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-02-oleaginous.csv"

      # case 03 - PROTEAGINOUS
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-03-proteaginous.csv"

      # case 04 - FIBER PLANT
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-04-fiber_culture.csv"

      # case 05 - FALLOW LAND
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-05-fallow_land.csv"

      # case 06 - LEGUMINOUS
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-06-leguminous.csv"

      # case 07 - FODDER LEGUMINOUS
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-07-fodder_leguminous.csv"

      # case 08 - FODDER
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-08-fodder.csv"

      # case 09 - TEMPORARY MEADOW
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-09-temporary_meadow.csv"

      # case 10 - PERMANENT MEADOW
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-10-pasture.csv"

      # case 11 - FRUITS AND VEGETABLES
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-11-fruits_and_vegetables.csv"

      # case 12 - ARBORICULTURE AND VITICULTURE
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-12-arboriculture_viticulture.csv"

      # case 13 - ORNEMENTAL PLANT
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-13-ornamental_plants.csv"

      # case 14 - MISCELLANEOUS
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-14-miscellaneous.csv"

      # case 15 - TROPICAL PLANT
      files << "#{CAP.abaci_dir}/v#{cap_year}/1-15-tropical_plants.csv"

      # METHODS
      #
      # files method to grab variety
      files.each do |file|
        f = File.open(file)
        table = Abaci.read(f)
        t = table.where(name: cap_land_parcel.main_crop_code)
        return t if t.any?
      end
      nil
    end

    def self.create_activity_production(cap_land_parcel, started_on = nil, stopped_on = nil, cap_year)
      # TEST in rails c
      # Ekylibre::Tenant.switch! 'demo'
      # cap_land_parcel = CapLandParcel.first
      # Ekylibre::Plugin::CAP::TelepacFile.create_activity_production(cap_land_parcel)
      #
      # CapLandParcel.find_each { |c| Ekylibre::Plugin::CAP::TelepacFile.create_activity_production(c,Date.civil(YYYY,MM,JJ)) }

      cap_year ||= 2020

      attributes = {
        cultivation_variety: "plant",
        support_variety: :land_parcel
      }

      # find row in abaci corresponding to cap_land_parcel

      row = find(cap_land_parcel, cap_year)
      unless row
        raise "Invalid row for CapLandParcel: #{cap_land_parcel.inspect}"
      end

      # update attributes from abaci
      attributes.update(cultivation_variety: row.first.variety.to_s, name: row.first.label_fr.to_s)

      # check in Lexicon
      if cap_year >= 2017
        year_col = "cap_#{cap_year}_crop_code"
        lexicon_production_nature = MasterProductionNature.where("#{year_col} = ?", cap_land_parcel.main_crop_code).first
      elsif cap_year < 2017
        lexicon_production_nature = MasterProductionNature.find_by(cap_2017_crop_code: cap_land_parcel.main_crop_code)
      end

      if lexicon_production_nature.present?
        attributes.update(
        name: lexicon_production_nature.human_name[Preference[:language].to_s],
        production_nature_id: lexicon_production_nature.id,
        cultivation_variety: lexicon_production_nature.specie
        )
      else
        attributes.update(name: row.first.label_fr.to_s)
      end

      # avoid activity_production creation for border, buffer and none
      unless %w[border buffer none].include? row.first.support_nature

        activity = Activity.find_by(attributes)
        activity ||= Activity.find_by(name: attributes[:name])

        if activity
          Rails.logger.info("Activity #{activity.name} already exist")
        else
          if !row.first.variety.nil? && (row.first.support_nature.nil? || (!row.first.support_nature.nil? && (row.first.support_nature.to_sym == :cultivation || row.first.support_nature.to_sym == :fallow_land)))
            attributes.update(
              family: :plant_farming,
              with_cultivation: true,
              with_supports: true,
              size_indicator: 'net_surface_area',
              size_unit: 'hectare',
              nature: :main
            )
          elsif row.first.variety.nil? && !row.first.support_nature.nil? && row.first.support_nature.to_sym != :cultivation
            attributes.update(
              family: :plant_farming,
              with_cultivation: false,
              with_supports: true,
              size_indicator: 'net_surface_area',
              size_unit: 'hectare',
              nature: :main
            )
          else
            raise "Invalid case for CapLandParcel: #{cap_land_parcel.inspect}"
          end
          attributes[:production_cycle] ||= :annual
          # puts attributes.inspect.green
          activity = Activity.create!(attributes)
        end

        # check if existing CultivableZone cover, overlap or intersect a cap_land_parcel
        cap_islet_shape = ::Charta.new_geometry(cap_land_parcel.islet.shape)
        cap_land_parcel_shape = ::Charta.new_geometry(cap_land_parcel.shape)
        # info = c.to_ewkt[0..100] + "...\n"
        cap_land_parcel_inside_cultivable_zone = CultivableZone.shape_covering(cap_land_parcel_shape, 0.05)
        unless cap_land_parcel_inside_cultivable_zone.any?
          # info << "Overlaps!\n"
          cap_land_parcel_inside_cultivable_zone = CultivableZone.shape_matching(cap_land_parcel_shape, 0.05)
          cap_land_parcel_inside_cultivable_zone ||= CultivableZone.shape_intersecting(cap_land_parcel_shape, 0.02)
        end

        # find or create a CultivableZone according to islet link to cap_land_parcel
        # TODO check correctly if cap_land_parcel is inside a cultivable_zone
        if cap_land_parcel_inside_cultivable_zone.any?
          # info << "Found\n"
          cultivable_zone = cap_land_parcel_inside_cultivable_zone.first
        else
          number = 'ZC#' + format('%02d', cap_land_parcel.islet_number.to_s)
          cultivable_zone = CultivableZone.find_or_initialize_by(work_number: number)
          cultivable_zone.name ||= cap_land_parcel.islet.city_name + " #" + format('%02d', cap_land_parcel.islet_number.to_s)
          cultivable_zone.shape ||= cap_islet_shape
          cultivable_zone.save!
        end

        # find or create an activity_production according to current informations
        # FIXME find_by_shape doesn't work...
        campaign = Campaign.find_or_create_by!(harvest_year: cap_year)
        productions = activity.productions.of_campaign(campaign).support_shape_matching(cap_land_parcel_shape, 0.02)
        activity_production = if productions.any?
                                productions.first
                              else
                                activity.productions.new(campaign: campaign)
                              end
        activity_production.support_shape = cap_land_parcel_shape
        activity_production.support_nature = (!row.first.support_nature.nil? ? row.first.support_nature.to_sym : :cultivation)
        activity_production.cultivable_zone = cultivable_zone
        activity_production.usage = row.first.usage

        # build started_on and stopped_on according to Lexicon production nature
        if lexicon_production_nature.present?
          year_gap = lexicon_production_nature.stopped_on.year - lexicon_production_nature.started_on.year
          started_on = DateTime.new(cap_year - year_gap, lexicon_production_nature.started_on.month, lexicon_production_nature.started_on.day).to_date
          stopped_on = DateTime.new(cap_year, lexicon_production_nature.stopped_on.month, lexicon_production_nature.stopped_on.day).to_date
        end

        activity_production.started_on = started_on
        activity_production.stopped_on = stopped_on
        activity_production.save!

        # link cap_land_parcel and activity_production
        cap_land_parcel.activity_production = activity_production
        cap_land_parcel.save!
      end
    end
  end
end
