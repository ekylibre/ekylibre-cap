module Telepac
  module V2019
    class CapStatementsExchanger < Telepac::Exchanger
      campaign 2019
      category :plant_farming
      vendor :telepac

      def check
        now = Time.zone.now
        valid = true

        # check if file is a valid XML
        f = File.open(file)
        # f = sanitize(f)
        doc = Nokogiri::XML(f, &:noblanks)
        valid
      end

      def import
        # import and parse file
        doc = Nokogiri::XML(File.open(file)) do |config|
          config.strict.nonet.noblanks
        end

        w.count = doc.css('parcelle').count

        #
        country_preference = Preference[:country]

        # get pacage number and campaign
        pacage_number = doc.at_css('producteur').attribute('numero-pacage').value

        statement_year = 2019
        campaign = Campaign.find_or_create_by!(harvest_year: statement_year)
        started_on ||= Date.civil(campaign.harvest_year, 1, 1)
        stopped_on ||= Date.civil(campaign.harvest_year, 12, 31)

        # get the exploitation siret_number
        siret_number = doc.at_css('demandeur siret').text

        # get global SRID
        first_town = doc.at_css('commune').text
        global_srid = ::CAP::SRS.find_srid(first_town)

        # get the exploitation name
        if exploitation_nature = doc.at_css('identification-societe exploitation')
          farm_name = exploitation_nature.text
        elsif exploitation_nature = doc.at_css('identification-individuelle identite')
          farm_name = exploitation_nature.text
        else
          raise "No farm name found in TelePAC folder."
        end

        # get the associates and make a link to entities
        # TODO

        # doc.at_css('demandeur iban').attribute('compte-iban').value
        # doc.at_css('demandeur iban').attribute('bic').value
        # doc.at_css('demandeur iban').attribute('titulaire').value

        ## find or create Entity
        declarant = Entity.find_by('last_name ILIKE ?', farm_name)
        unless declarant
          declarant = Entity.create!(last_name: farm_name, active: true, nature: :organization, country: country_preference, siret_number: siret_number)
        end

        cap_statement_attributes = {
          campaign: campaign,
          declarant: declarant,
          farm_name: farm_name,
          pacage_number: pacage_number,
          siret_number: siret_number
        }

        ## find and update or create cap statement
        cap_statement = CapStatement.find_by(cap_statement_attributes.slice(:pacage_number, :siret_number, :declarant, :campaign))
        cap_statement = CapStatement.new unless cap_statement
        cap_statement.attributes = cap_statement_attributes
        cap_statement.save!

        # get the islets
        doc.css('ilot').each do |islet|
          # get islet attributes
          # islet number and town_number
          islet_number = islet.attribute('numero-ilot').value
          town_number = islet.css('commune').text

          srid = ::CAP::SRS.find_srid(town_number)

          # islet shape, validate GML and transform into Charta
          geometry = islet.xpath('.//gml:Polygon')
          geometry.first['srsName'] = "EPSG:#{srid}"
          geom = ::Charta.from_gml(geometry.first.to_xml.to_s.squish, srid).transform(:WGS84).convert_to(:multi_polygon)

          islet_attributes = {
            cap_statement: cap_statement,
            islet_number: islet_number,
            town_number: town_number,
            shape: geom.to_rgeo
          }

          # find and update or create islet according to cap statement
          cap_islet = CapIslet.find_by(islet_attributes.slice(:islet_number, :cap_statement))
          cap_islet = CapIslet.new unless cap_islet
          cap_islet.attributes = islet_attributes
          cap_islet.save!

          # import into georeadings
          label = 'I' + '-' + cap_islet.cap_statement.pacage_number.to_s + '-' + cap_islet.cap_statement.campaign.harvest_year.to_s + '-' + cap_islet.islet_number.to_s
          georeadings_attributes = {
            name: label,
            number: label,
            nature: :polygon,
            content: cap_islet.shape
          }
          unless georeading = Georeading.find_by(georeadings_attributes.slice(:number))
            georeading = Georeading.create!(georeadings_attributes)
          end

          # get cap_land_parcels
          islet.css('parcelle').each do |land_parcel|
            # get land_parcel attributes
            land_parcel_number = land_parcel.css('descriptif-parcelle').attribute('numero-parcelle').value
            main_crop_seed_production = land_parcel.css('culture-principale').attribute('production-semences').value
            # main_crop_commercialisation = land_parcel.css('culture-principale').attribute('commercialisation').value
            main_crop_code = land_parcel.css('culture-principale > code-culture').text
            main_crop_precision = land_parcel.css('precision').text

            # land_parcel shape, validate GML and transform into Charta
            geometry = land_parcel.xpath('.//gml:Polygon')
            geometry.first['srsName'] = "EPSG:#{srid}"
            geom = ::Charta.from_gml(geometry.first.to_xml.to_s.squish, srid).transform(:WGS84).convert_to(:multi_polygon)

            cap_land_parcel_attributes = {
              cap_islet: cap_islet,
              land_parcel_number: land_parcel_number,
              main_crop_code: main_crop_code,
              # main_crop_commercialisation: main_crop_commercialisation,
              main_crop_precision: main_crop_precision,
              main_crop_seed_production: main_crop_seed_production,
              shape: geom.to_rgeo
            }

            # find and update or create land_parcel according to cap statement and islet
            if cap_land_parcel = CapLandParcel.find_by(cap_land_parcel_attributes.slice(:land_parcel_number, :cap_islet))
              cap_land_parcel.update_attributes!(cap_land_parcel_attributes)
            else
              cap_land_parcel = CapLandParcel.create!(cap_land_parcel_attributes)
            end

            # import into georeadings
            label = 'P' + '-' + cap_land_parcel.islet.cap_statement.pacage_number.to_s + '-' + cap_land_parcel.cap_islet.cap_statement.campaign.harvest_year.to_s + '-' + cap_land_parcel.islet_number.to_s + '-' + cap_land_parcel.land_parcel_number.to_s
            georeadings_attributes = {
              name: label,
              number: label,
              nature: :polygon,
              content: cap_land_parcel.shape
            }
            unless georeading = Georeading.find_by(georeadings_attributes.slice(:number))
              georeading = Georeading.create!(georeadings_attributes)
            end

            # create activity and production
            CAP::TelepacFile.create_activity_production(cap_land_parcel, started_on, stopped_on, statement_year)

            w.check_point
          end
        end

        # get SNA
        doc.css('sna-declaree').each do |sna|
          if sna.css('numeroSna').text.blank?
            sna_number = sna.css('numeroSnacreationTas').text
          else
            sna_number = sna.css('numeroSna').text
          end
          sna_category = sna.css('categorieSna').text
          sna_nature = sna.css('typeSna').text

          geometry = sna.xpath('.//gml:Polygon')
          if !geometry.empty?
            geometry.first['srsName'] = "EPSG:#{global_srid}"
            geom = ::Charta.from_gml(geometry.first.to_xml.to_s.squish, global_srid).transform(:WGS84).convert_to(:multi_polygon)
          end

          geometry = sna.xpath('.//gml:Point')
          if !geometry.empty?
            geometry.first['srsName'] = "EPSG:#{global_srid}"
            geom = ::Charta.from_gml(geometry.first.to_xml.to_s.squish, global_srid).transform(:WGS84).convert_to(:point)
          end

          cap_neutral_area_attributes = {
            cap_statement_id: cap_statement.id,
            number: sna_number,
            category: sna_category,
            nature: sna_nature,
            shape: geom.to_rgeo
          }

          # find and update or create neutral_area according to cap statement
          if cap_neutral_area = CapNeutralArea.find_by(cap_neutral_area_attributes.slice(:cap_statement_id, :number))
            cap_neutral_area.update_attributes!(cap_neutral_area_attributes)
          else
            cap_neutral_area = CapNeutralArea.create!(cap_neutral_area_attributes)
          end
        end


      end

      def sanitize(xml)
        # TODO: validate telepac xml using xsd/xslt (if any)
        xml.to_s.squish
      end


    end
  end
end
