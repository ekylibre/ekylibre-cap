module Telepac
  class << self
    def exchangers
      ActiveExchanger::Base.exchangers.values
        .select { |e| e < Telepac::Exchanger }
        .map { |e| [e.campaign, e] }
        .sort_by(&:first)
        .to_h
    end
  end

  class Exchanger < ActiveExchanger::Base
    vendor :telepac

    class << self
      def campaign(campaign = nil)
        return @campaign unless campaign
        @campaign = campaign
      end
    end
  end
end
